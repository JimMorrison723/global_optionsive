﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shell;

namespace csgo_movie
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string Steampath;

        public string SteamPath
        {
            get { return Steampath; }
            set { Steampath = value; }
        }

        public string CSGOPath
        {
            get { return SteamPath + "/SteamApps/common/Counter-Strike Global Offensive/csgo/"; }
        }

        public MainWindow()
        {
            InitializeComponent();
            GetSteamPath();
            CheckSettingsFile();
            CheckConfigFile();
        }

        private void CheckConfigFile()
        {
            try
            {
                string path = CSGOPath + @"cfg/csgo_movie.cfg";
                if (!File.Exists(path))
                {
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine("echo \"|||MOVIE CONFIG|||\"");
                        sw.WriteLine("sv_cheats 1");
                        sw.WriteLine("cl_draw_only_deathnotices 1");
                        sw.WriteLine("// Your movie (high quality) config here (paste or use: exec <configname.cfg)");
                        sw.WriteLine("// Fore example:");
                        sw.WriteLine("mat_queue_mode 0");
                        sw.WriteLine("mat_postprocess_enable 0");
                        sw.WriteLine("mat_dxlevel 90");
                        sw.WriteLine("");
                        sw.WriteLine("echo \"csgo_movie.cfg loaded\"");
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("You don't have csgo installed? :(", "bumm, problem", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                throw;
            }
        }

        private void CheckSettingsFile()
        {
            try
            {
                string path = CSGOPath + "\\csgomovie.txt";
                if (!File.Exists(path))
                {
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine(SteamPath);
                        sw.WriteLine("-novid -console");
                        sw.WriteLine("-windowed -w 1280 -h 720 +exec csgo_movie.cfg");
                        sw.WriteLine("");
                    }
                }

                LoadSettingsFile(path);
            }
            catch (Exception)
            {
                MessageBox.Show("You don't have csgo installed?", "bumm, problem", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                throw;
            }

        }

        private void LoadSettingsFile(string path)
        {
            string sor = "";
            using (StreamReader sr = new StreamReader(path))
            {
                sor = sr.ReadLine();
                if (CSGOPath != sor)
                    SteamPath = sor;

                lbl_csgo_path.Content = CSGOPath;
                txtB_default.Text = sr.ReadLine();
                txtB_movie.Text = sr.ReadLine();
                txtB_custom.Text = sr.ReadLine();
            }
        }

        private void GetSteamPath()
        {
            try
            {
                SteamPath = (string)Registry.GetValue("HKEY_CURRENT_USER\\Software\\Valve\\Steam", "SteamPath", "0");
                if (SteamPath != "0")
                    lbl_csgo_path.Content = SteamPath;
                else
                    lbl_csgo_path.Content = "CS:GO not found!";
            }
            catch (Exception)
            {
                MessageBox.Show("Can't find steam on your computer (nothing in regedit)!", "CS:GO not installed?", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                throw;
            }
        }

        private void save_settings(object sender, RoutedEventArgs e)
        {
            SaveSettingsFile();
        }

        private void SaveSettingsFile()
        {
            try
            {
                string path = CSGOPath + "\\csgomovie.txt";
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(lbl_csgo_path.Content);
                    sw.WriteLine(txtB_default.Text);
                    sw.WriteLine(txtB_movie.Text);
                    sw.WriteLine(txtB_custom.Text);
                }

                CreateJumpList(txtB_default.Text, txtB_movie.Text, txtB_custom.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Can't save config file!", "Problem", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                throw;
            }
        }

        private void CreateJumpList(string p1, string p2, string p3)
        {
            string path = SteamPath + "/steam.exe";

            //Uri iconUri=new Uri("pack://application:,,,/TheExternalAssembly;component/Resouces/ak.ico");

            JumpList jumpList = new JumpList();
            //JumpList.SetJumpList(Application.Current, jumpList);

            //jumpList.JumpItems.Clear();
            //GetType().Assembly.GetManifestResourceNames();

            JumpTask gamemode = new JumpTask();
            gamemode.Title = "Default mode";
            gamemode.Description = p1; //"Start CS:GO as usual."
            gamemode.ApplicationPath = path;
            gamemode.Arguments = "-applaunch 730 " + p1;
            gamemode.IconResourcePath = CSGOPath + "csgo.exe";
            //gamemode.IconResourcePath = (string)Application.Current.FindResource("ak");
            gamemode.IconResourceIndex = -1;
            jumpList.JumpItems.Add(gamemode);

            JumpTask moviemode = new JumpTask();
            moviemode.Title = "Recording mode";
            moviemode.Description = p2; //"Start CS:GO with your movie settings."
            moviemode.ApplicationPath = path;
            moviemode.Arguments = "-applaunch 730 " + p2;
            moviemode.IconResourcePath = @"%windir%\system32\msinfo32.exe";
            moviemode.IconResourceIndex = 0;
            jumpList.JumpItems.Add(moviemode);

            JumpTask custommode = new JumpTask();
            custommode.Title = "Custom mode";
            custommode.Description = p3; //"Start CS:GO with your custom settings."
            custommode.ApplicationPath = path;
            custommode.Arguments = "-applaunch 730 " + p3;
            custommode.IconResourcePath = @"%windir%\system32\notepad.exe";
            custommode.IconResourceIndex = 0;
            jumpList.JumpItems.Add(custommode);


            jumpList.ShowFrequentCategory = false;
            jumpList.ShowRecentCategory = false;

            JumpList.SetJumpList(Application.Current, jumpList);
            jumpList.Apply();
        }

        private void StartCSGO(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveSettingsFile();

                string parameters = "";
                if (r_movie.IsChecked == true)
                    parameters = txtB_movie.Text.ToString();
                else
                    parameters = txtB_default.Text.ToString();

                Process.Start(SteamPath + "/steam.exe", "-applaunch 730" + parameters);

            }
            catch (Exception)
            {
                MessageBox.Show("Can't start cs:go!", "Problem", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                throw;
            }
        }

        private void check_update(object sender, MouseButtonEventArgs e)
        {
            if (this.Content.ToString() == "Update available!")
            {
                Process.Start("http://jimmorrison723.hu");
            }
            else
            {
                lbl_update.Content = "Checking for update...";
                lbl_update.Foreground = new SolidColorBrush(Colors.Orange);
                try
                {
                    WebClient client = new WebClient();
                    int vn = Convert.ToInt32(client.DownloadString("http://jimmorrison723.hu/program_updates/csgo_quicklaunch/update.txt"));
                    string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();

                    if (vn > Convert.ToInt32(version.Replace(".", "")))
                    {
                        lbl_update.Content = "Update available!";
                        lbl_update.Foreground = new SolidColorBrush(Colors.Pink);
                    }
                    else
                    {
                        lbl_update.Content = "Up-to-date!";
                        lbl_update.Foreground = new SolidColorBrush(Colors.Green);
                    }
                }
                catch (Exception)
                {
                    lbl_update.Content = "Coudn't retrieve data";
                    lbl_update.Foreground = new SolidColorBrush(Colors.Red);
                    throw;
                }
            }
        }

        private void open_webpage(object sender, MouseButtonEventArgs e)
        {
            Process.Start("http://jimmorrison723.hu");
        }

        private void btn_help_click(object sender, RoutedEventArgs e)
        {
            var HelpWindow = new HelpWindow();
            HelpWindow.Show();
        }
    }
}